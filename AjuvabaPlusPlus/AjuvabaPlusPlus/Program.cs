﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AjuvabaPlusPlus
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 7;
            int y = 8;
            y++;
            ++y;

            Console.WriteLine($"x on {x} ja y on {y}");

            Console.WriteLine(x++);
            Console.WriteLine(y++);

            Console.WriteLine($"x on {x} ja y on {y}");
            // siiani on loogiline
            Console.WriteLine($"x++ on {x++} ja y++ on {y++}");
            Console.WriteLine($"x on {x} ja y on {y}");
            Console.WriteLine($"++x on {++x} ja ++y on {++y}");
            Console.WriteLine($"x on {x} ja y on {y}");

            // alustame otsast aga teisiti
            x = 4; y = 9;
            Console.WriteLine($"x on {x} ja y on {y}");
            Console.WriteLine("teeme y = x++");
            y = (x++);
            Console.WriteLine($"x on {x} ja y on {y}");
            Console.WriteLine("teeme y = ++x");
            y = (++x);
            Console.WriteLine($"x on {x} ja y on {y}");


        }
    }
}
