﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tsüklid
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 7,2,3,8,9,1,4,8,9 };


            for (int i = arvud.Length; i > 0; )
            //for (int i = 0; i < arvud.Length; i++)
            {
                Console.WriteLine(arvud[--i]);
                
            }

            //foreach (var x in arvud)
            //{
            //    Console.WriteLine(x);
            //}

            // vaatame nüüd üht teist moodi tsüklit - while

            Console.Write("Tead mis, ");
            string vastus = "";
            //for (; vastus != "ostan";)  
            //while (vastus != "ostan")
            while (vastus != "ostan") 
            {
                Console.Write("osta elevant ära: ");
                vastus = Console.ReadLine();
                if (vastus != "ostan") Console.Write($"Kõik ütlevad, et {vastus}, aga ");
            } 

            Console.WriteLine("tubli, elevant on nüüd sinu");

            

        }
    }
}
