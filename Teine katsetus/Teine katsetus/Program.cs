﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teine_katsetus
{
    class Program
    {
        static void Main(string[] args)
        {
            // @"
            // $"
            // $@"
            string failinimi = @"..\..\nimekiri.txt";

            //var loetud = File.ReadAllText(failinimi);
            //Console.WriteLine(loetud);

            var loetudRead = File.ReadAllLines(failinimi);
            foreach (var x in loetudRead)
            {
                Console.WriteLine(x);
            }

            //string uusfailinimi = @"..\..\uusnimekiri.txt";
            //File.WriteAllLines(uusfailinimi, loetudRead);

            // meil on nüüd massiiv kus (mitu?) rida
            // igas reas nimi (mistüüpi) ja vanus (mistüüpi)
            // kuidas saada see dictionary (nimed, vanused) ???

            Dictionary<string, int> nimekiri = new Dictionary<string, int>();
            foreach(var x in loetudRead)
            {
                // siin peaks kuidagi selle loetudrea (Henn 64)
                // pistma sellesse dictionary nimekiri.Add(nimi, vanus)

                // ÕIGE :) string vaja teha juppideks
                // ja see teine jupp oleks omakorda vaja intiks teha

                // 1. samm - teeme juppideks
                var jupid = x.Split(' ');
                string nimi = jupid[0];
                int vanus = int.Parse(jupid[1]);
                nimekiri.Add(nimi, vanus);
            }

            foreach (var x in nimekiri) Console.WriteLine(x);
            Console.WriteLine($"keskmine vanus on {nimekiri.Values.Average()}");

              


        }
    }
}
