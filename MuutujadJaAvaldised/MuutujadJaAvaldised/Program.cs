﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuutujadJaAvaldised
{
    class Program
    {
        static void Main(string[] args)
        {
            // tegin nüüd uue projekti - õhtuks läheb see üles
            // selle leiab minu nime all 
            // https://gitlab.com/Hennsarv/aug19-first-week.git
            // kes tahab, võib omale kloonida ja proovida ja eeskujundada


            int a = 3;
            int b = 10;
            b += 7;
            int c;

            // TODO: siin peaks veel midagi lisama

            Console.WriteLine(c = b + a);

            Console.WriteLine(++c); // mis siin trükitakse

            // elvis tehte näide
            Console.WriteLine("täna lähen " + (
                DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "trenni"
                : DateTime.Now.DayOfWeek == DayOfWeek.Tuesday ? "laulma"
                : DateTime.Now.DayOfWeek == DayOfWeek.Saturday ? "sauna"

                : "õlutit jooma"
                ));

            // ma jätan täna stringid ja stringitehted hoopis homseks
            // siin on nii jube palav, et ma ise annan varsti otsad :)
            // ma korraks lähen kempsu ukse taha värsket õhku hingama


        }

        /// <summary>
        /// See asi liidab teisi asju omavahel kokku
        /// </summary>
        /// <param name="x">esimene liidetav</param>
        /// <param name="y">teine liidetav</param>
        /// <returns>summa</returns>
        public static int Liida(int x, int y)
        {
            return x + y;
        }
    }
}
