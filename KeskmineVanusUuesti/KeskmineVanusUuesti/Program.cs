﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeskmineVanusUuesti
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> nimekiri = new Dictionary<string, int>();
            while(true)
            {
                Console.Write("Anna nimi: ");
                string nimi = Console.ReadLine();
                if (nimi == "") break;
                Console.Write($"anna {nimi} vanus: ");
                int vanus = int.Parse(Console.ReadLine());
                nimekiri.Add(nimi, vanus);
            }

            Console.WriteLine("Keskmine vanus on {0}", nimekiri.Values.Average());
            int maxVanus = nimekiri.Values.Max();

            foreach(var x in nimekiri)
                if(x.Value == maxVanus)
                    Console.WriteLine($"Vanim on {x.Key} ta on {x.Value} aastane");




        }
    }
}
