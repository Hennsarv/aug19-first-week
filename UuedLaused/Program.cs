﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UuedLaused
{
    class Program
    {
        static void Main(string[] args)
        {
            // täna õpime mõned uued laused
            // muidu läheb asi igavaks

            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday) 
            {
                // neid lauseid siin täidetakse vaid kolmapäeviti
                Console.WriteLine("Jee! nädala keskel oleme");
                int a = 3;
                Console.WriteLine(a);
            }
            else if (DateTime.Now.DayOfWeek == DayOfWeek.Friday) 
            {
                // neid lauseid täidetakse reedel
                Console.WriteLine("kohe saab maale jooma minna");
            }
            else
            {
                // neid lauseid täidetakse AINULT kõigil muudel päevadel
                Console.WriteLine("igav");
                string a = "Ahaa";
                Console.WriteLine(a);
            }

            Console.WriteLine("tavaline päev");
            // siit edasi juba iga päev
            Console.WriteLine("veel üks aga kavalam lause - switch");

            switch(DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Wednesday:
                case DayOfWeek.Friday:
                    // kolmapäevased ja reedesed toimetused
                    Console.WriteLine("läheme trenni");
                    break;
                case DayOfWeek.Saturday:
                    Console.WriteLine("läheme sauna");
                    // laupäevased toimetused
                    goto case DayOfWeek.Sunday;
                case DayOfWeek.Monday:
                    Console.WriteLine("väike peaparandus");
                    goto default;
                case DayOfWeek.Sunday:
                    // siia pühapäevased asjad
                    Console.WriteLine("joome õlutit");
                    break;
                default:
                    // muude päevade toimetused
                    Console.WriteLine("teeme tööd");
                    break;
            }


        }
    }
}
