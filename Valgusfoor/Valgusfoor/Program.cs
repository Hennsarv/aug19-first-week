﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valgusfoor
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Kõigepealt, mis tegema peab
             * 1. Küsima ekraanilt värvi (enne vist printima küsimuse)
             * 2. Analüüsima saadud vastust
             * 3. Vastavalt vastusele reageerima
             * 
             */

            // 1. küsime mis värvi näeb:


            //bool oota = true;
            while (true)
            {
                Console.Write("Mis värv seal valgusfooris on: ");
                //string värv = Console.ReadLine();

                // miski peaks kontrollima nüüd - kas teeme iffiga või switchiga?

                switch (Console.ReadLine().ToLower())
                {
                    case "green": // mis tehti rohelisega
                    case "roheline": // mis tehti rohelisega
                        Console.WriteLine("sõida julgelt edasi");
                        //oota = false;
                        return;
                    case "yellow": // mis teha kollasega
                    case "kollane": // mis teha kollasega
                        Console.WriteLine("oota rohelist");
                        break;
                    case "red":
                    case "punane":
                        Console.WriteLine("jää kohe seisma");
                        break;
                    default:
                        Console.WriteLine("osta prillid");
                        break;
                }

            }
            // vaja miskit muuta
            // lisame eestikeelsed ja kuidagi peaks suured-väikesed tähed saama?
            // eestikeelsed saime
            // kuidas suurtest tähtedest jagu saada?
            // miks mul üldse see muutuja??
            // wow - saingi tööle
        }
    }
}
