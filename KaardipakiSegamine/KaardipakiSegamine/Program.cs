﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardipakiSegamine
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("\nesimene variant paneme puuduvad\n");
            int[] pakk = new int[52];
            Random r = new Random(5138100);
            DateTime algus = DateTime.Now;
            int mitu = 0;
            do
            {
                int uus = r.Next(52);
                if (!pakk.Contains(uus)) pakk[mitu++] = uus;
                
            }
            while (mitu < 51);
            for (int i = 0; i < pakk.Length; i++)
            {
                Console.Write($"{(i%4==0?"\n":"\t")}{pakk[i]}");
            }
            Console.WriteLine("\n\nteine variant \"segamisega\"\n");
            // see seal on esimene variant
            DateTime stop1 = DateTime.Now;
            pakk = Enumerable.Range(0, 52).ToArray(); // praegu on järjest

            for (int i = 0; i < 10000; i++)
            {
                int a = r.Next(52);
                int b = r.Next(52);
                int c = pakk[a]; pakk[a] = pakk[b]; pakk[b] = c;
            }
            for (int i = 0; i < pakk.Length; i++)
            {
                Console.Write($"{(i % 4 == 0 ? "\n" : "\t")}{pakk[i]}");
            }
            DateTime stop2 = DateTime.Now;
            Console.WriteLine("\n\nkolmas variant tõstmisega\n");

            List<int> pakkList = Enumerable.Range(0, 52).ToList(); // pakk segamata (aga list)
            List<int> segatudPakk = new List<int>();

            while(pakkList.Count > 0)
            {
                int a = r.Next(pakkList.Count);
                segatudPakk.Add(pakkList[a]);
                pakkList.RemoveAt(a);
            }
            for (int i = 0; i < segatudPakk.Count; i++)
            {
                Console.Write($"{(i % 4 == 0 ? "\n" : "\t")}{segatudPakk[i]}");
            }
            DateTime stop3 = DateTime.Now;
            Console.WriteLine();

            Console.WriteLine("variant1 kestis {0} ms", (stop1 - algus).Milliseconds);
            Console.WriteLine("variant2 kestis {0} ms", (stop2 - stop1).Milliseconds);
            Console.WriteLine("variant3 kestis {0} ms", (stop3 - stop2).Milliseconds);

        }
    }
}
