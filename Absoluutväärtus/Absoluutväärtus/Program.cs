﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Absoluutväärtus
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("anna üks arv (võib ka miinusega olla): ");
            int arv = int.Parse(Console.ReadLine());
            //arv = arv < 0 ? -arv : arv;       // või nii
            //arv *= (arv < 0 ? -1 : 1);        // vahel on kasulikkum teha nii
            //arv = Math.Abs(arv);              // enamasti tehakse nii

            Console.WriteLine(arv);

        }
    }
}
