﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ÕpilasedJaVanused
{
    class Program
    {
        static void Main(string[] args)
        {
            // siia vahele hakkan kirjutama
            // mida ma kõigepealt peaks tegema
            const int ÕPILASTE_ARV = 20;
            string[] nimed = new string[ÕPILASTE_ARV];
            int[] vanused = new int[ÕPILASTE_ARV];

            int õpilasteArv = 0;
            for (; õpilasteArv < ÕPILASTE_ARV; õpilasteArv++)
            {
                // küsime nime
                Console.Write($"Anna õpilase nr {õpilasteArv} nimi: ");
                nimed[õpilasteArv] = Console.ReadLine();
                // kui tühi vastus, siis edasi ei küsi
                if (nimed[õpilasteArv] == "") break;
                // siis küsime vanus
                Console.Write($"Anna {nimed[õpilasteArv]} vanus: ");
                vanused[õpilasteArv] = int.Parse(Console.ReadLine());
            }

            Console.WriteLine($"õpilasi on {õpilasteArv}");

            double vanusteSumma = 0;
            for (int i = 0; i < õpilasteArv; i++) vanusteSumma += vanused[i];
            double keskmineVanus = vanusteSumma / õpilasteArv;
            Console.WriteLine($"keskmine vanus on {keskmineVanus:F2}");

            // kästi leida, kes on kõige vanem
            // kuidas seda teha???

            // tsükliga mis tõugu, mis tingimusel ja mis seal sees peale hakata
            int vanimVanus = vanused[0];
            int kesOnVanim = 0;

            for (int i = 1; i < õpilasteArv; i++)
            {
                if (vanused[i] > vanimVanus)
                // vanimVanus = vanused[kesOnVanim = i];
                {
                    kesOnVanim = i;
                    vanimVanus = vanused[i];
                }
            }
                Console.WriteLine($"vanim on {nimed[kesOnVanim]}");

        }
    }
}
