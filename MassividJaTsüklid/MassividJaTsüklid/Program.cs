﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassividJaTsüklid
{
    class Program
    {
        static void Main(string[] args)
        {
            // siiani oli elu lihtne!
            // nüüd läheb keeruliseks :O
            // võtke tükk kohvi enne

            int arv = 7; // muutujal on 1 väärtus igal hetkel
            // vahel tahaks rohkem 

            int[] arvud = new int[10]; // sellel muutujal võib olla terve rida väärtusi
            // ja teda kutsutakse massiiviks

            //arvud = new int[10]; // nüüd on sellel asjal 10 väärtust
            arvud[4] = 78; // viies neist on nüüd 78
            arvud[3]++; // neljas neist on nüüd 1

            int[] teised1 = new int[5] { arv, arv * 2, arv * 3, arv * 4, 7 };
            int[] teised2 = new int[] { arv, arv * 2, arv * 3, arv * 4, 7 };
            int[] teised3 = { arv, arv * 2, arv * 3, arv * 4, 7 };

            arvud = new int[] { 1, 2, 3 };

            arvud = teised2;

            teised2[0]++;
            Console.WriteLine(arvud[0]);

            Console.WriteLine("massiivis on {0} arvu", arvud.Length);

            int[][] seeOnImelikLoom = { new int[3], new int[7], new int[10] };
            int[][] imelikLoom = { new int[] { 1, 2 }, new int[] { 1, 2, 3, 4, 5 }, new int[] { 5, 6 } };

            int[,] tabel = new int[4, 7];
            int[,] tabel2 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
            Console.WriteLine(tabel.Length); // palju on massiivis elemente
            Console.WriteLine(tabel.Rank);   // mitmemõõtmeline on massiiv
            Console.WriteLine(tabel.GetLength(0));  // mitu "veergu" on massiivis
            Console.WriteLine(tabel.GetLength(1));  // mitu "rida" on 2-mõõtmelises

            int[][][][][][][][][][] vägaJaburasi;
            int[,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,] kaVägaJaburasi;
            // massiivil võib olla kuitahes palju "mõõtmeid"
            // seda nii mitmemõõtmelisel kui massiivide massiivil
            // aga üle 3 mõõtme puhul palun too arstitõend!
            int[,,][,,][,,] hoobisJabur;   // parem unusta ära :)
            // enamasti piirdutakse ühemõõtmelise massiiviga
            // ja ka massiiv kipub ununema oma moodsate uute sugulaste kõrval
            // List, Dictionary, Stack, Queue ... jne jne jne (neist tuleb juttu)

            // massividega saab teha põnevaid tehteid - kõiki me ei jõua vaadata
            // kiika Array-klassi meetodeid     toksi Array. ja vaata, mis pakutakse
            // kiika massiivi enda meetodeid    toksi arvud. ja vaata, mis pakutakse

            int[] esimene = { 1, 2, 3 };
            int[] teine = { 2, 7, 8 };
            esimene = esimene.Union(teine).ToArray();

            Console.WriteLine(esimene);


        }
    }
}
