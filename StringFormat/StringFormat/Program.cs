﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringFormat
{
    class Program
    {
        static void Main(string[] args)
        {
            // mis asi on ConsoleWrite ja string format
            Console.WriteLine("See tekst läheb ekraanile");

            int i = 77;
            double d = Math.PI; 
            DateTime täna = DateTime.Today;

            Console.WriteLine("Meie i on " +i.ToString());
            Console.WriteLine("meie d on " + d.ToString());
            Console.WriteLine("meie d on {0:F4}", d);
            Console.WriteLine(täna.ToString("(ddd) dd.MMMM yyyy"));
            Console.WriteLine($"Täna on {täna:dddd}");

        }
    }
}
