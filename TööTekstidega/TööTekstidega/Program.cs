﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TööTekstidega
{
    class Program
    {
        static void Main(string[] args)
        {
            // üks andmetüüp on TEKSTIJADA selle nimi on string

            string nimi = "Henn";
            string teinenimi = "Sarv";

            nimi = nimi + " " + teinenimi;
            Console.WriteLine(nimi);

            teinenimi = "Henn SARV";
            Console.WriteLine(nimi == teinenimi);
            Console.WriteLine(nimi.Equals(teinenimi)); // Javas PEAB nii tegema, meil on ka ilus nii
            Console.WriteLine(nimi.Equals(teinenimi, StringComparison.CurrentCultureIgnoreCase));
            // Equalsiga saab võrrelda mitut moodi
            // Equals on objektidel üleüldine võrdlemise funktsioon

            //string lause = @"Luts kirjutab: ""Kui Arno isaga ..."" ja nii edasi";

            //string filename = "c:\\henn\\text.txt";
            //string teinefilename = @"C:\Users\sarvi\Source\Repos\Esimene nädal\TööTekstidega\TööTekstidega";
            //Console.WriteLine(lause);

            int vanus = 64;
            Console.WriteLine(nimi + " on " + vanus + " aastat vana");  // 1. liitmine (halb)
            Console.WriteLine("{0} on {1} aastat vana", nimi, vanus);   // 2. formaatimisega

            string formatEst = "{0} on {1} aastat vana";
            string formatFin = "{1} vuotta vanha on tämä {0} poikaa";

            string format = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday
                ? formatFin : formatEst;


            string teade = String.Format(format, nimi, vanus);
            Console.WriteLine(teade);               // 3. formaatimisfunktsiooniga

            Console.WriteLine("{0} on tore poiss kuigi {0} on {1} aastat juba vana", nimi, vanus);

            teade = string.Format("{0} on {1} aastat vana", nimi, vanus);
            teade = $"{nimi} on {vanus} aastat vana";
            // need kaks lauset on IDENTSED!

            Console.WriteLine(teade);

            Console.WriteLine(nimi);
            Console.WriteLine(nimi.ToUpper());
            Console.WriteLine(nimi.ToLower());

            // stringiga saab teha hulga tehteid
            // teisendada suureks täheks, väikseks täheks
            Console.WriteLine(nimi.Split(' ')[1]); // teha tükkideks
            Console.WriteLine(nimi.Substring(5,4)); // võtta jupikesi NB! JAVAS jälle teisiti

            string selline = "see on reavahetusega \n string";


        }
    }
}
