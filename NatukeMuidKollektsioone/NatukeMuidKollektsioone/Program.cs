﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatukeMuidKollektsioone
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 1, 2, 3, 4, 5, 6, 7 };
            int[] ruudud = new int[10];
            //for (int i = 0; i < ruudud.Length; i++) ruudud[i] = i * i;
            //foreach (var x in ruudud) Console.WriteLine(x);

            List<int> arvudelist = new List<int>(100) { 1, 2, 3, 4, 5, 6, 7 };

            // list erineb massiivist, sets sinna saab juurde pann aja ära võtta
            arvudelist.Add(14);
            arvudelist.Remove(4);
            arvudelist.Remove(6);
            arvudelist.RemoveAt(1);

            foreach (var x in arvud) Console.WriteLine(x);
            foreach (var x in arvudelist) Console.WriteLine(x);

            //for (int i = 1000; i < 2000; i++) arvudelist.Add(i);

            arvudelist.AddRange(Enumerable.Range(800, 1000)); // see lisab mul listi arvud 800..1799

            Console.WriteLine(arvudelist.Count);    // count ütleb palju meid on
            Console.WriteLine(arvudelist.Capacity); // capacity ütleb, palju me ruumi võtame

            SortedSet<int> sorditud = new SortedSet<int> { 1, 7, 2, 8, 3, 1, 9, 2, 14 };
            foreach (var x in sorditud) Console.WriteLine(x);

            // sorted setis on vaid unikaalsed tegelased ja järjekorras

            var teine = arvudelist.ToArray();

            // dictionary sisaldab võtme-väärtuse paare
            Dictionary<string, int> nimekiri = new Dictionary<string, int>();

            nimekiri.Add("Henn", 64);
            nimekiri.Add("Ants", 28);
            nimekiri.Add("Peeter", 40);

            // ja neile saab ligi vätme järgi
            nimekiri["Ants"] = 128;
            nimekiri["Kalle"] = 33;

            foreach (var x in nimekiri) Console.WriteLine(x);

            Dictionary<string, string> sõnad = new Dictionary<string, string>()
            {
                {"love", "armastus" },
                {"hate", "viha" },
                {"money", "raha" },
                {"cristmastree", "jõulupuu" },
            };


            Console.WriteLine(nimekiri.Values.Average());

            // nimekirjas saab kontrollida, kas "võti" juba on seal olemas
            if (! nimekiri.ContainsKey("Kalle")) nimekiri.Add("Kalle", 33);


            Random r = new Random(); // see on kahh üks tore loom, selle käest saab küsida juhuslikke arve


            SortedSet<int> juhuslikud = new SortedSet<int>();
            //for (int i = 0; i < 1000; i++)
            {
                juhuslikud.Add(r.Next(100000));   // r.next(100000) annab juhusliku arvu vahemikus 0..999999
            }

            

            var juhuslikudlist = juhuslikud.ToList();

            foreach (var x in juhuslikud) Console.WriteLine(x);
            int mitu = juhuslikudlist.Count;
            if(mitu % 2 == 0)
                Console.WriteLine($"mediaan on {(juhuslikudlist[mitu/2] + juhuslikudlist[mitu/2-1]) / 2 }");
            else
                Console.WriteLine($"mediaan on {juhuslikudlist[mitu/2]}");


        }
    }
}
