﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValgusfoorNiiNaguMeVeelEiOska
{
    class Program
    {
        static void Main(string[] args)
        {
            var variandid = new Dictionary<string, string>()
            {
                {"roheline", "sõida edasi" },
                {"green", "lets go" },
                {"yellow", "wait green" },
                {"kollane", "oota rohelist" },
                {"punane", "jää seisma" },
                {"red", "stop" },
            };

            Console.Write("mis värv / whats color: ");

            try
            {
                Console.WriteLine(variandid[Console.ReadLine().ToLower()]);
            }
            catch (Exception)
            {
                Console.WriteLine("osta prillid");
            }
        }

    }
}
